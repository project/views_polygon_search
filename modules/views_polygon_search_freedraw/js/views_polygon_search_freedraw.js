/**
 * @file
 * Some basic behaviors and utility functions for Views.
 */
const viewsPolygonSearchControl = L.Control.extend({
  _freeDraw: {},
  _addButton: undefined,
  _removeButton: undefined,
  _removeAllButton: undefined,
  _polygons: [],

  _createButton: function (title, className, container) {
    const link = L.DomUtil.create('a', className, container);
    link.href = '#';
    link.title = title;
    L.DomEvent
      .on(link, 'click', L.DomEvent.preventDefault);
    return link;
  },
  _getWKT: function (polygons) {
    const wktPolygons = [];
    for (let i = 0; i < polygons.length; i++) {
      const coords = [];
      const latlngs = polygons[i];
      for (let num = 0; num < latlngs.length; num++) {
        coords.push(latlngs[num].lng + " " + latlngs[num].lat);
      }
      wktPolygons.push("POLYGON((" + coords.join(", ") + "," + coords[i] + "))");
    }
    return wktPolygons;
  },

  _addPolygon: function() {
    this._freeDraw.mode(LeafletFreeDraw.CREATE);
  },

  _removePolygon: function() {
    this._freeDraw.mode(LeafletFreeDraw.DELETE);
  },
  _removeAllPolygons: function() {
    for (let i = 0; i < this._polygons.length; i++) {
      this._map.removeLayer(this._polygons[i])
    }
    this._polygons = [];
    this._freeDraw.clear();
  },

  _enableButton: function (button, fn, context) {
    if (button && !button.buttonEnabled) {
      const stop = L.DomEvent.stopPropagation;
      L.DomEvent
        .on(button, 'click', stop)
        .on(button, 'touchstart', stop)
        .on(button, 'mousedown', stop)
        .on(button, 'dblclick', stop)
        .on(button, 'click', fn, context);
      L.DomUtil.removeClass(button, 'enabled');
      L.DomUtil.addClass(button, 'enabled');
      button.buttonEnabled = true;
      button.buttonDisabled = false;
    }
  },

  _disableButton: function (button, fn, context) {
    if (button && !button.buttonDisabled) {
      const stop = L.DomEvent.stopPropagation;
      L.DomEvent
        .off(button, 'click', stop)
        .off(button, 'mousedown', stop)
        .off(button, 'touchstart', stop)
        .off(button, 'dblclick', stop)
        .off(button, 'click', fn, context);
      L.DomUtil.removeClass(button, 'enabled');
      button.buttonEnabled = false;
      button.buttonDisabled = true;
    }
  },

  _initButtons: function () {
    const polygons = this._freeDraw.all(true);
    if (polygons.length > 0) {
      this._enableButton(this._removeButton, this._removePolygon, this);
      this._enableButton(this._removeAllButton, this._removeAllPolygons, this);
      if (!this.options.multiple) {
        this._disableButton(this._addButton, this._addPolygon, this);
      }
    }
    else {
      this._enableButton(this._addButton, this._addPolygon, this);
      this._disableButton(this._removeButton, this._removePolygon, this);
      this._disableButton(this._removeAllButton, this._removeAllPolygons, this);
    }
  },
  getColor() {
    const reg=/^#([0-9a-f]{3}){1,2}$/i;
    return this.options.color && reg.test(this.options.color)
      ? this.options.color
      : '';
  },
  onAdd: function (map) {
    const self = this;
    this._freeDraw = new LeafletFreeDraw.freeDraw({
      mode: LeafletFreeDraw.CREATE | LeafletFreeDraw.DELETE | LeafletFreeDraw.ALL,
      mergePolygons: false,
    });

    this._freeDraw.on("markers", function (e) {
      self._initButtons();

      if (e.eventType === "create" && e.latLngs.length > 0) {
        const latLngs = e.latLngs[e.latLngs.length - 1];
        const polygon = L.polygon(
          latLngs.map(function(latLng) {
            return [latLng.lat,latLng.lng];
          }), {
            className: "leaflet-polygon",
            color: self.getColor(),
          });

        polygon.on("click", function () {
          if (self._freeDraw.mode() === LeafletFreeDraw.DELETE) {
            const current_polygon = this;
            self._polygons = self._polygons.filter(function (item) {
              return current_polygon._leaflet_id !== item._leaflet_id;
            })
            self._freeDraw.remove(this);
          }
        });

        polygon.addTo(map);
        self._polygons.push(polygon);
      }

      const wktPolygons = self._polygons.map((polygon) => {
        return self._getWKT(polygon._latlngs)
      });
      const event = new CustomEvent("FreeDraw:change", {detail: {
          polygons: wktPolygons,
          domId: self.options.domId,
          textAreaId: self.options.textAreaId
        }});
      document.body.dispatchEvent(event);
    });
    map.addLayer(this._freeDraw);

    const container = L.DomUtil.create('div', 'views-polygon-search-control');
    this._addButton =  this._createButton(
      Drupal.t('Add polygon'), 'add-polygon',  container);
    if (this.options.buttons && this.options.buttons.removeOne) {
      this._removeButton = this._createButton(
        Drupal.t('Remove polygon'), 'remove-polygon',  container);
    }
    if (this.options.buttons && this.options.buttons.removeAll) {
      this._removeAllButton = this._createButton(
        Drupal.t('Remove all polygons'), 'remove-all-polygon',  container);
    }

    this._initButtons();
    return container;
  }
});

(function ($, Drupal) {
  Drupal.behaviors.viewsPolygonSearch = {
    attach: function (context, settings) {
      settings.viewsPolygonSearch.forEach(function (v) {
          once('views-poligon-search', '.js-view-dom-id-' + v.domId).forEach((view) => {
          const $view = $(view);
          const map = $('.view-content > div[id*=leaflet-map]', $view);
          const mapData = map.data("leaflet");
          const drawControll = new viewsPolygonSearchControl(v);
          drawControll.addTo(mapData.lMap);
          Drupal.behaviors.viewsPolygonSearch.addInitialPolygons(mapData.lMap, $view, drawControll);
        })

      });
      once('free-draw-change', 'body').forEach((body) => {
        $(body).on('FreeDraw:change', function (e) {
          if (typeof e.originalEvent.detail != "undefined") {
            const details = e.originalEvent.detail;
            const view = $('.js-view-dom-id-' + details.domId);
            const $textAreaId = $('.views-polygon-search-' + details.textAreaId, view);
            const wkt = details.polygons === undefined
              ? ""
              : e.originalEvent.detail.polygons.join("+");
            $textAreaId.val(wkt);
          }
        });
      })
    },
    addInitialPolygons(lMap, container, leafletFreeDraw) {
      const $polygonTextarea = container.find('.view-filters .views-polygon-search').first();
      if ($polygonTextarea.val()) {
        const polygonsStrs = $polygonTextarea.val().split('+') || [];
        polygonsStrs.forEach(function (str) {
          const layer_wkt = new Wkt.Wkt();
          layer_wkt.read(str);
          if (layer_wkt.toJson().coordinates && layer_wkt.toJson().coordinates[0]) {
            const latLngs = layer_wkt.toJson().coordinates[0].map(function (item) {
              return {
                lng: item[0],
                lat: item[1],
              }
            });
            leafletFreeDraw._freeDraw.create(latLngs);
          }
        });
      }
    }
  };
})(jQuery, Drupal);
