<?php

namespace Drupal\views_polygon_search_google_freedraw\Plugin\ViewsPolygonSearchPlugin;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Random;
use Drupal\Core\Form\FormState;
use Drupal\views_polygon_search\ViewsPolygonSearchPluginBase;

/**
 * Widget for google maps polygon search filter.
 *
 * @ViewsPolygonSearchPlugin(
 *   id = "google_freedraw",
 *   label = @Translation("Google FreeDraw"),
 * )
 */
class ViewsPolygonSearchGoogleFreedraw extends ViewsPolygonSearchPluginBase {

  /**
   * {@inheritdoc}
   */
  public function formOptions(&$form, FormStateInterface $form_state, array $options) {
    $positions = [
      'TOP_LEFT' => $this->t('Top left of the map.'),
      'TOP_RIGHT' => $this->t('Top right of the map.'),
      'BOTTOM_LEFT' => $this->t('Bottom left of the map.'),
      'BOTTOM_RIGHT' => $this->t('Bottom right of the map.'),
    ];

    $form['draw_position'] = [
      '#title' => $this->t('Draw button position'),
      '#type' => 'select',
      '#options' => $positions,
      '#default_value' => isset($positions[$options['draw_position']]) ? $options['draw_position'] : 'TOP_LEFT',
    ];

    $form['reset_position'] = [
      '#title' => $this->t('Reset button position'),
      '#type' => 'select',
      '#options' => $positions,
      '#default_value' => isset($positions[$options['reset_position']]) ? $options['reset_position'] : 'TOP_LEFT',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function valueForm(&$form, FormState $form_state, $handler) {
    $options = $handler->options['filter_plugin_style']['options'] ?? [];
    $random = new Random();
    $unique = $random->name();
    $form['value'] += [
      '#attributes' => [
        'class' => [
          'views-polygon-search',
          'views-polygon-search-' . $unique,
        ],
      ],
      '#attached' => [
        'library' => ['views_polygon_search_google_freedraw/views_polygon_search_google_freedraw'],
        'drupalSettings' => [
          'viewsPolygonSearchGoogle' => [
            [
              'domId' => $handler->view->dom_id,
              'textAreaId' => $unique,
              'draw_position' => $options['draw_position'] ?? 'TOP_LEFT',
              'reset_position' => $options['reset_position'] ?? 'TOP_LEFT',
              'multiple' => ($handler->operator != '='),
            ],
          ],
        ],
      ],
    ];
  }

}
