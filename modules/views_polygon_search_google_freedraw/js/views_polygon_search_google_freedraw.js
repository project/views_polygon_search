(function (Drupal, drupalSettings) {

  Drupal.viewsPolygonSearchGoogle = {
    features: [],
    searchField: null,
    /**
     * Clears the map contents.
     */
    clearMap: function () {
      let i;

      // Reset the remembered last string (so that we can clear the map,
      // paste the same string, and see it again)
      this.searchField.last = '';

      for (i in this.features) {
        if (this.features.hasOwnProperty(i)) {
          this.features[i].setMap(null);
        }
      }
      this.features.length = 0;
    },
    /**
     * Clears the current contents of the textarea.
     */
    clearText: function () {
      this.searchField.value = '';
    },
    /**
     * Updates the textarea based on the first available feature.
     */
    updateText: function () {
      const wkt = new Wkt.Wkt();
      wkt.fromObject(this.features[0]);
      this.searchField.value = wkt.write();
    },
  }

  Drupal.behaviors.viewsPolygonSearchGoogle = {
    attach: function (context, settings) {
      if (drupalSettings['geofield_google_map']) {
        let mapObserver = null;
        if ('IntersectionObserver' in window) {
          mapObserver = new IntersectionObserver(function (entries, observer) {
            for (let i = 0; i < entries.length; i++) {
              if (entries[i].isIntersecting) {
                const mapId = entries[i].target.id;
                Drupal.behaviors.viewsPolygonSearchGoogle.loadMap(mapId, element);
              }
            }
          });
        }

        once('views-polygon-search-google', 'html .geofield-google-map').forEach(function (element) {
          const mapId = element.id;
          if (drupalSettings['geofield_google_map'][mapId]) {
            const map_settings = drupalSettings['geofield_google_map'][mapId]['map_settings'];
            if (mapObserver && map_settings['map_lazy_load']['lazy_load']) {
              mapObserver.observe(element)
            }
            else {
              Drupal.behaviors.viewsPolygonSearchGoogle.loadMap(mapId, element);
            }
          }
        });
      }
    },
    loadMap(mapId, element) {
      if (drupalSettings['geofield_google_map'][mapId]) {

        const map_settings = drupalSettings['geofield_google_map'][mapId]['map_settings'];

        // Set the map_data[mapid] settings.
        Drupal.geoFieldMapFormatter.map_data[mapId] = map_settings;

        // Load before the GMap Library, if needed.
        Drupal.geoFieldMapFormatter.loadGoogle(mapId, map_settings.gmap_api_key, map_settings.map_additional_libraries, function () {
          if (!document.getElementById(mapId)) {
            return;
          }

          Drupal.behaviors.viewsPolygonSearchGoogle.mapInitialize(mapId, element);
        });
      }
    },
    mapInitialize(mapId, element) {
      const gmap = Drupal.geoFieldMapFormatter.map_data[mapId].map;

      let googleSearchSettings;
      drupalSettings.viewsPolygonSearchGoogle.forEach(function (s) {
        if (document.querySelector(`.js-view-dom-id-${s.domId} #${mapId}`)) {
          googleSearchSettings = s;
          return false;
        }
      });

      gmap.drawingManager = new google.maps.drawing.DrawingManager({
        drawingControlOptions: {
          position: google.maps.ControlPosition[googleSearchSettings.draw_position || 'TOP_LEFT'],
          drawingModes: [
            google.maps.drawing.OverlayType.POLYGON
          ]
        },
        polygonOptions: gmap.defaults
      });
      gmap.drawingManager.setMap(gmap);

      const googleSearch = Drupal.viewsPolygonSearchGoogle;
      googleSearch.searchField = element.closest('.view-id-map').querySelector('.views-polygon-search');

      // Create the DIV to hold the control and call the mapResetControl()
      // constructor passing in this DIV.
      const mapDrawControlDiv = document.createElement('div');
      mapDrawControlDiv.style.zIndex = "10";
      mapDrawControlDiv.index = 1;
      const resetButton = Drupal.behaviors.viewsPolygonSearchGoogle.mapResetControl(mapDrawControlDiv, mapId);
      const mapDrawControlPosition = googleSearchSettings.reset_position || 'TOP_LEFT';
      gmap.controls[google.maps.ControlPosition[mapDrawControlPosition]].push(mapDrawControlDiv);

      resetButton.addEventListener('click', function () {
        googleSearch.clearText();
        googleSearch.clearMap();
      });

      google.maps.event.addListener(gmap.drawingManager, 'overlaycomplete', function (event) {
        googleSearch.clearText();
        googleSearch.clearMap();

        // Set the drawing mode to "pan" (the hand) so users can immediately edit
        this.setDrawingMode(null);

        // Polygon drawn
        if (event.type === google.maps.drawing.OverlayType.POLYGON || event.type === google.maps.drawing.OverlayType.POLYLINE) {
          // New vertex is inserted
          google.maps.event.addListener(event.overlay.getPath(), 'insert_at', function (n) {
            googleSearch.updateText();
          });

          // Existing vertex is removed (insertion is undone)
          google.maps.event.addListener(event.overlay.getPath(), 'remove_at', function (n) {
            googleSearch.updateText();
          });

          // Existing vertex is moved (set elsewhere)
          google.maps.event.addListener(event.overlay.getPath(), 'set_at', function (n) {
            googleSearch.updateText();
          });
        } else if (event.type === google.maps.drawing.OverlayType.RECTANGLE) { // Rectangle drawn
          // Listen for the 'bounds_changed' event and update the geometry
          google.maps.event.addListener(event.overlay, 'bounds_changed', function () {
            googleSearch.updateText();
          });
        }

        googleSearch.features.push(event.overlay);
        const wkt = new Wkt.Wkt();
        wkt.fromObject(event.overlay);
        googleSearch.searchField.value = wkt.write();
      });
    },
    mapResetControl(mapDrawControlDiv, mapId) {
      // Set CSS for the control border.
      const controlUI = document.createElement('div');
      controlUI.style.backgroundColor = '#fff';
      controlUI.style.boxShadow = 'rgba(0,0,0,.3) 0px 1px 4px -1px';
      controlUI.style.cursor = 'pointer';
      controlUI.title = Drupal.t('Click to reset the map to its initial state');
      controlUI.style.margin = '10px';
      controlUI.style.position = 'relative';
      controlUI.id = 'geofield-map--' + mapId + '--reset-control';
      mapDrawControlDiv.appendChild(controlUI);

      // Set CSS for the control interior.
      const controlText = document.createElement('div');
      controlText.style.position = 'relative';
      controlText.innerHTML = Drupal.t('Reset Polygons');
      controlText.style.padding = '0 17px';
      controlText.style.display = 'table-cell';
      controlText.style.height = '40px';
      controlText.style.fontSize = '18px';
      controlText.style.color = 'rgb(86,86,86)';
      controlText.style.textAlign = 'center';
      controlText.style.verticalAlign = 'middle';
      controlUI.appendChild(controlText);

      return controlUI;
    },
  };
})(Drupal, drupalSettings);
