## CONTENTS OF THIS FILE

- Introduction
- Requirements
- Installation
- Configuration


## INTRODUCTION

The Views Polygon Search module is a module that allow search by polygon.
The Views Polygon Search FreeDraw submodule allows to draw polygon to search.

## REQUIREMENTS

This module requires geofield, search_api, drupal:views modules.


## INSTALLATION

The installation of this module is like other Drupal modules.

1. If your site is [managed via Composer](https://www.drupal.org/node/2718229),
   use Composer to download the Views Polygon Search module running
   ```composer require "drupal/views_polygon_search"```. Otherwise copy/upload the views_polygon_search
   module to the modules directory of your Drupal installation.

2. Enable the 'Views Polygon Search' module and desired sub-modules in 'Extend'.
   (`/admin/modules`)


## CONFIGURATION

- Add geofield to some entity type.
- Create view for this entity type.
- Use Leaflet Map view style.
- Add Polygon filter.
